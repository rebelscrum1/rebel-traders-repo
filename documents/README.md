#Doc folder to store any project related items such as build notes, architecture, designs etc.

##Clark##
Command I have been using to run the build after traderdb database schema was created and table script executed.
Then on local (not remote) linux machine EG: Putty clone repo for the OrderBroker and run docker command below:

docker run -p 8080:61616 -p 8161:8161 -d --restart unless-stopped --name mqbroker dockerreg.conygre.com:5000/mqbroker:latest

Back on the local machine the java run command is:

java -DSERVER_PORT=8081 -DPROFILES=default -DAPP_LOG_LEVEL=INFO -DDB_HOST=localhost -DDB_PORT=3306 -DDB_SCHEMA=traderdb -DDB_USER=root -DDB_PASS=c0nygre -DMQ_DOCK_NUM=17 -jar spring-auto-trader-0.0.1-SNAPSHOT.jar

NOTE: above command is 1st version, may change so check properties file!
