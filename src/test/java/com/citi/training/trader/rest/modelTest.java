package com.citi.training.trader.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;

import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Stock;
import java.util.Date;

public class modelTest {
	
	private int testId = 32;
	Stock testStock = new Stock(-1, "TES");
	private double testPrice = 22.36;
	private Date testRecordedAt = new Date();
	private int testStockId = 45;
	private String testTicker = "Goog";
	private int testSimpleId = 7;
	private Stock testSimpleStock = new Stock(-1, "GOOG");
	private int testSize = 5;
	private double testExitProfitLoss = 21.30;
	private int testCurrentPosition = 3;
	private double testLastTradePrice = 12.70;
	private double testProfit = 50.71;
	private Date testStopped = new Date();
	
	
	
	@Test
	public void testing_Price_constructor() {
		Price testingPrice = new Price(testId, testStock, testPrice, testRecordedAt);
		assertEquals(testId, testingPrice.getId());
		assertEquals(testStock, testingPrice.getStock());
		assertEquals(testPrice, testingPrice.getPrice(), 0.0001);
		assertEquals(testRecordedAt, testingPrice.getRecordedAt());
		
	}
	
	
	@Test
    public void test_Price_toString() {
		String testString = new Price (testId, testStock, testPrice, testRecordedAt).toString();
		assertTrue(testString.contains((new Integer(testId)).toString()));
		assertTrue(testString.contains(String.valueOf(testStock)));
		assertTrue(testString.contains(String.valueOf(testPrice)));
		assertTrue(testString.contains(String.valueOf(testRecordedAt)));
		
    }
	
	@Test
	public void test_Stock_constructor() {
		Stock testingStock = new Stock(testStockId, testTicker);
		assertEquals(testStockId, testingStock.getId());
		assertEquals(testTicker, testingStock.getTicker());
		
	}
	
	@Test
	public void test_Stock_toString() {
		String testStockString = new Stock (testStockId, testTicker).toString();
		assertTrue(testStockString.contains((new Integer(testStockId)).toString()));
		assertTrue(testStockString.contains(testTicker));
	}

	@Test
	public void test_SimpleStrategy_constructor() {
		SimpleStrategy testingSimpleStrategy = new SimpleStrategy(testSimpleId, testSimpleStock,testSize,testExitProfitLoss,testCurrentPosition,
				testLastTradePrice,testProfit,testStopped);
		assertEquals(testSimpleId,testingSimpleStrategy.getId());
		assertEquals(testSimpleStock,testingSimpleStrategy.getStock());
		assertEquals(testSize,testingSimpleStrategy.getSize());
		assertEquals(testExitProfitLoss,testingSimpleStrategy.getExitProfitLoss(),0.0001);
		assertEquals(testCurrentPosition,testingSimpleStrategy.getCurrentPosition());
		assertEquals(testLastTradePrice,testingSimpleStrategy.getLastTradePrice(),0.0001);
		assertEquals(testProfit,testingSimpleStrategy.getProfit(),0.0001);
		assertEquals(testStopped,testingSimpleStrategy.getStopped());
		
		
	}
	
	@Test
	public void test_SimpleStrategy_toString() {
		String testSimpleString = new SimpleStrategy(testSimpleId, testSimpleStock,testSize,testExitProfitLoss,testCurrentPosition,
				testLastTradePrice,testProfit,testStopped).toString();
		assertTrue(testSimpleString.contains((new Integer(testSimpleId).toString())));
		assertTrue(testSimpleString.contains(String.valueOf(testSimpleStock)));
		assertTrue(testSimpleString.contains((new Integer(testSize).toString())));
		assertTrue(testSimpleString.contains(String.valueOf(testExitProfitLoss)));
		assertTrue(testSimpleString.contains((new Integer(testCurrentPosition).toString())));
		//assertTrue(testSimpleString.contains((new Double(testLastTradePrice).toString())));
		//assertTrue(testSimpleString.contains((new Double(testProfit).toString())));
		assertTrue(testSimpleString.contains(String.valueOf(testStopped)));
		
	}
}
