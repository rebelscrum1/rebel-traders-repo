package com.citi.training.trader.rest;

import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.service.AverageStrategyService;
import com.citi.training.trader.service.PriceService;
import org.apache.commons.math3.util.Precision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("${com.citi.training.trader.rest.stock-base-path:/api/price}")
public class PriceController {

    @Autowired
    AverageStrategyService averageStrategyService;

    private static final Logger logger =
            LoggerFactory.getLogger(PriceController.class);

    @RequestMapping(value="/{stock}/{upper}/{lower}", method= RequestMethod.GET,
            produces= MediaType.APPLICATION_JSON_VALUE)
    public List<Price> findById(@PathVariable String stock, @PathVariable String upper, @PathVariable String lower) {

        return averageStrategyService.findAll(stock,upper,lower);
    }

//    @RequestMapping(value="/{id}", method=RequestMethod.GET,
//            produces=MediaType.APPLICATION_JSON_VALUE)
//    public Trade findById(@PathVariable int id) {
//        logger.debug("findById(" + id + ")");
//        return PriceService.findById(id);
//    }
}
