package com.citi.training.trader.service;

import com.citi.training.trader.dao.AverageStrategyDao;
import com.citi.training.trader.dao.PriceDao;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Stock;
import org.apache.commons.math3.util.Precision;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AverageStrategyService {

    @Autowired
    private AverageStrategyDao averageStrategyDao;

    @Autowired
    private PriceDao priceDao;

    public List<SimpleStrategy> findAll(){
        return averageStrategyDao.findAll();
    }

    public List<Price> findAll(String stock, String upperDate, String lowerDate) {
        return priceDao.findAll(stock,upperDate,lowerDate);
    }
    public List<Price> findAll(Stock stock) {
        return priceDao.findAll(stock);
    }

    public Double getPriceAverage(List<Price> list){
        double sum=0.0;
        for (Price price:list) {
             sum=+price.getPrice();
        }
        return Precision.round(sum/list.size(),4);
    }
}
