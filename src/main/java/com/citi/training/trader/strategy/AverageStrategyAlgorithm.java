package com.citi.training.trader.strategy;

import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.service.AverageStrategyService;
import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

@Component
public class AverageStrategyAlgorithm {

    @Autowired
    private AverageStrategyService averageStrategyService;

    private List<Price> priceList = new ArrayList<>();
    private CircularFifoQueue<Double> longAvgHistory = new CircularFifoQueue<Double>(2);
    private CircularFifoQueue<Double> shortAvgHistory = new CircularFifoQueue<Double>(2);

    public String wrapperMethod(SimpleStrategy strategy){
        return compareAverage(getShortAverage(strategy),getLongAverage(strategy));
    }

    private Double getShortAverage(SimpleStrategy strategy){
        LocalDateTime currentTime =new LocalDateTime ();
        LocalDateTime otherTime = currentTime.minusMinutes(2);
        String s = currentTime.toString("yyyy-MM-dd HH:mm:ss");
        String o= otherTime.toString("yyyy-MM-dd HH:mm:ss");

        priceList=averageStrategyService.findAll(strategy.getStock().getTicker(),s,o);

        return averageStrategyService.getPriceAverage(priceList);
    }

    private Double getLongAverage(SimpleStrategy strategy){
        LocalDateTime currentTime =new LocalDateTime ();
        LocalDateTime otherTime = currentTime.minusMinutes(4);
        String s = currentTime.toString("yyyy-MM-dd HH:mm:ss");
        String o= otherTime.toString("yyyy-MM-dd HH:mm:ss");

        priceList=averageStrategyService.findAll(strategy.getStock().getTicker(),s,o);

        return averageStrategyService.getPriceAverage(priceList);
    }


    private String compareAverage(Double shortAverage, Double longAverage) {
    	
    	 Random rand = new Random();
         int random= rand.nextInt(10);

         if((random%2)==0){
             shortAverage= shortAverage-4;
         }else{
             longAverage= longAverage+4;
         }

        this.longAvgHistory.add(longAverage);
        this.shortAvgHistory.add(shortAverage);
        String tradeDecision = "";

        if(longAvgHistory.isAtFullCapacity()) {
            if (this.shortAvgHistory.get(1) < this.longAvgHistory.get(0) &&
                    this.shortAvgHistory.get(0) > this.longAvgHistory.get(0)) {

                tradeDecision = "BUY";

            } else if (this.shortAvgHistory.get(1) > this.longAvgHistory.get(0) &&
                    this.shortAvgHistory.get(0) < this.longAvgHistory.get(0)) {

                tradeDecision = "SELL";

            }
            return tradeDecision;
        }
        return "Pending";

    }

    public Map<String,Double> getAverages(SimpleStrategy strategy) {

        Map<String,Double> averages = new HashMap<>();

        averages.put("ShortAverage",getShortAverage(strategy));
        averages.put("LongAverage",getLongAverage(strategy));

        return averages;
    }
}
