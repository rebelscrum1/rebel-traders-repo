import { Component, OnInit } from '@angular/core';
import { TraderService } from './trader.service';
import { DeprecatedI18NPipesModule } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'trader-ui';
 trades:Object[] = [{cat:'AAPL'}, {cat: 'GOOG'}, {cat: 'BRK-A'}, {cat:'NSC'}, {cat:'MSFT'} ]
 whichCategory:string | boolean
 model
 showFilter = false
 showFilterToday = false
 showLatest = false

  ngOnInit(){
    this.showFilterToday=false
    this.showFilter = false
    this.invokeService();
  }

  constructor(private traderService:TraderService){}

  handleClick(){
    this.showFilter=false
    this.showFilterToday=false
    this.showLatest = false
    this.invokeService();
   
  }
  // handleClickSearch(){
  //   this.traderService.getDataParams(this.whichCategory) .subscribe((result)=>{this.model=result})
  // }

  dateStr1
  fullDate
  handleTodayClick(){
    this.showFilterToday = true
    this.showFilter=false
    this.showLatest = true

    this.dateStr1 = new Date().toISOString().substr(0, 10);
    this.fullDate = this.dateStr1+'T00:00:00'
    console.log(this.fullDate)
  }

  handleChange(){
    this.showFilter=true
    this.showFilterToday=false
    this.showLatest = true
    

  }

  allTrades;
  invokeService(){
    this.traderService.getData().subscribe( (result)=>{
      this.allTrades = result
      console.log(result)} );
  }

}
